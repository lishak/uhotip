<?php

namespace App\Presenters;

use Nette;
use App\Model;

/**
 * Api presenter.
 */
class ApiPresenter extends BasePresenter
{

    public function actionGetSports()
    {
        $result = array();

        $sports = $this->context->sport->getAll();
        foreach ($sports as $idSport => $name) {
            $leagues = $this->context->league->getAllBySport($idSport);
            $result[] = array('name' => $name, 'leagues' => $leagues, 'id' => $idSport);
        }

        $this->sendJson($result);
        $this->terminate();
    }

    public function actionGetRounds($idLeague)
    {
        $result = array();
        $rounds = $this->context->league->getRounds($idLeague);
        foreach ($rounds as $idRound => $round) {
            $result[] = array(
                'name' => $round->name,
                'from' => $round->from->format('d.m.Y'),
                'to' => $round->to->format('d.m.Y'),
                'id' => $idRound
            );
        }

        $this->sendJson($result);
        $this->terminate();
    }

    public function actionGetMatches($idRound, $idLeague)
    {
        $result = array();
        $matches = $this->context->match->getByRoundAndLeague($idRound, $idLeague);
        foreach ($matches as $idMatch => $match) {
            $result[$idMatch] = array(
                'name' => $match->name,
                'date' => $match->date->format('d.m.Y H:i')
            );
        }
        $this->sendJson($result);
        $this->terminate();
    }

    public function actionSignIn()
    {
        try {
            $postdata = file_get_contents("php://input");
            $request = json_decode($postdata);

            $user = $request->user;
            $login = $user->login;
            $password = $user->password;

            $this->getUser()->login($login, $password);
            $user = array();

            if ($this->getUser()->isLoggedIn()) {
                $user = $this->context->userModel->getById($this->getUser()->getId());
            }
            $this->sendJson($user);

        } catch (\Nette\Security\AuthenticationException $exception) {
            $this->sendJson(array());
        }

        $this->terminate();
    }

    public function actionRegister($login, $password)
    {
        $this->user->authenticator->add($login, $password);
        $this->terminate();
    }

    public function actionSignOut()
    {
        $this->user->logout();

        $this->sendJson(array());
        $this->terminate();
    }
}
