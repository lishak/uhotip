<?php
namespace App\Presenters;

use Nette;
use App\Model;
use Nette\Security\IUserStorage;

/**
 * Base presenter for all application presenters.
 */
abstract class BasePresenter extends Nette\Application\UI\Presenter
{
    /** @var \App\Security\AccessControlList @inject */
    public $acl;

    public function startup()
    {
        parent::startup();

        if ($this->getName() != 'Sign' && $this->getAction() != 'in') {
            if (!$this->user->isLoggedIn()) {
                if ($this->user->getLogoutReason() === IUserStorage::INACTIVITY) {
                    $this->flashMessage('Session timeout, you have been loget out', 'warning');
                }

                $this->redirect('Sign:in', array('backlink' => $this->storeRequest()));
            } elseif (!$this->acl->isAllowed($this->getName(), $this->getAction())) {
                    $this->flashMessage('Access denied', 'danger');
                    $this->redirect(':Homepage:');
            }
        }
    }
}
