<?php
namespace App\Model;

class League extends Base implements IModel
{
    /** @var string */
    protected $tableName = 'leagues';

    /**
     * Get all leagues
     *
     * @param number|null $limit
     * @param number|null $offset
     * @return array
     */
    public function getAll($limit = null, $offset = null)
    {
        $rows = $this->findAll($limit, $offset);

        $result = array();
        foreach ($rows as $row) {
            $result[$row->id] = $row->name;
        }

        return $result;
    }

    /**
     * Get all leagues by sport ID
     *
     * @param number $idSport
     * @param number|null $limit
     * @param number|null $offset
     * @return array
     */
    public function getAllBySport($idSport, $limit = null, $offset = null)
    {
        $rows = $this->findAll($limit, $offset)
            ->where('sports_id', $idSport);

        $result = array();
        foreach ($rows as $row) {
            $result[$row->id] = $row->name;
        }

        return $result;
    }

    public function getRounds($idLeague)
    {
        $result = $this->database->queryArgs(
            'SELECT r.*
            FROM rounds AS r
            JOIN matches AS m ON (r.id = m.rounds_id )
            JOIN leagues AS l ON (l.id = m.leagues_id AND l.id=?)
            WHERE r.`to` > NOW()
            ORDER BY r.`from`',
            array($idLeague)
        )->fetchAll();

        return $result;
    }
}
