<?php
namespace App\Model;

class Sport extends Base implements IModel
{
    /** @var string */
    protected $tableName = 'sports';

    /**
     * Get all sports
     *
     * @param number|null $limit
     * @param number|null $offset
     * @return array
     */
    public function getAll($limit = null, $offset = null)
    {
        $rows = $this->findAll($limit, $offset);
        $result = array();
        foreach ($rows as $row) {
            $result[$row->id] = $row->name;
        }

        return $result;
    }
}
