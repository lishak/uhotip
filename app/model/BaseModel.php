<?php
namespace App\Model;

abstract class Base extends \Nette\Object
{
    /** @var \Nette\Database\Context */
    public $database;
    /** @var string */
    protected $tableName;

    /**
     * @param \Nette\Database\Connection $db
     * @throws \Nette\InvalidStateException
     */
    public function __construct(\Nette\Database\Context $db)
    {
        $this->database = $db;

        if ($this->tableName === null) {
            $class = get_class($this);
            throw new \Nette\InvalidStateException("Table name must be defined in $class::\$tableName.");
        }
    }

    /**
     * Get table
     *
     * @return \Nette\Database\Table\Selection
     */
    protected function getTable()
    {
        return $this->database->table($this->tableName);
    }

    /**
     * Find all rows from table
     *
     * @return \Nette\Database\Table\Selection
     */
    public function findAll($limit = null, $offset = null)
    {
        if (is_null($limit)) {
            return $this->getTable();
        }

        return $this->getTable()->limit($limit, $offset);
    }

    /**
     * Find rows by array
     * example: array('name' => 'David') table past in SQL query WHERE name = 'David'
     *
     * @param array $by
     * @return \Nette\Database\Table\Selection
     */
    public function findBy(array $by)
    {
        return $this->getTable()->where($by);
    }

    /**
     * Find row by array
     *
     * @param array $by
     * @return \Nette\Database\Table\ActiveRow|FALSE
     */
    public function findOneBy(array $by)
    {
        return $this->findBy($by)->limit(1)->fetch();
    }

    /**
     * Find row by id
     *
     * @param int $id
     * @return \Nette\Database\Table\ActiveRow|FALSE
     */
    public function find($id)
    {
        return $this->getTable()->get($id);
    }

    /**
     * Update row
     *
     * @param array $data
     */
    public function update($data)
    {
        $this->findBy(array('id' => $data['id']))->update($data);
    }

    /**
     * Insert new row
     * @param array $data
     * @return \Nette\Database\Table\ActiveRow
     */
    public function insert($data)
    {
        return $this->getTable()->insert($data);
    }

    /**
     * Delete row by id
     * @param number $id
     */
    public function delete($id)
    {
        $this->getTable()->where('id', $id)->delete();
    }
}
