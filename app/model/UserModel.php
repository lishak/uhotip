<?php
namespace App\Model;

class Users extends Base implements IModel
{
    /** @var string */
    protected $tableName = 'users';

    /**
     * Get all sports
     *
     * @param number|null $limit
     * @param number|null $offset
     * @return array
     */
    public function getAll($limit = null, $offset = null)
    {
        $rows = $this->findAll($limit, $offset);
        $result = array();
        foreach ($rows as $row) {
            $result[$row->id] = $row->login;
        }

        return $result;
    }

    public function getById($id)
    {
        $row = $this->findOneBy(array('id' => $id));

        $result = array();
        $result['id'] = $id;
        $result['login'] = $row->login;
        $result['name'] = $row->name;
        $result['points'] = $row->points;

        return $result;
    }
}
