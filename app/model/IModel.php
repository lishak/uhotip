<?php
namespace App\Model;

interface IModel
{
    /**
     * @param $limit
     * @param $offset
     */
    public function getAll($limit, $offset);
}
