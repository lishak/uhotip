<?php
namespace App\Model;

class Match extends Base implements IModel
{
    /** @var string */
    protected $tableName = 'matches';

    /**
     * Get all sports
     *
     * @param number|null $limit
     * @param number|null $offset
     * @return array
     */
    public function getAll($limit = null, $offset = null)
    {
        $rows = $this->findAll($limit, $offset);
        $result = array();
        foreach ($rows as $row) {
            $result[$row->id] = $row->name;
        }

        return $result;
    }

    public function getByRoundAndLeague($idRound, $idLeague, $limit = null, $offset = null)
    {
        $rows = $this->findAll($limit, $offset)->where('rounds_id = ? AND leagues_id = ?', $idRound, $idLeague);
        $result = array();
        foreach ($rows as $row) {
            $result[$row->id] = $row;
        }

        return $result;
    }
}
