<?php
namespace App\Security;

use Nette\Security\IIdentity;

class Identity implements IIdentity
{
    /** @var number */
    protected $id;
    /** @var string */
    protected $role;
    /** @var string[] */
    protected $permissions;
    /** @var string */
    protected $login;

    public function __construct($id, $role, $login, $permissions)
    {
        $this->id = $id;
        $this->role = $role;
        $this->login = $login;
        $this->permissions = $permissions;
    }

    /**
     * @return number
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getRole()
    {
        return $this->role;
    }

    /**
     * @return string[]
     */
    public function getRoles()
    {
        return array($this->role);
    }

    /**
     * @return \string[]
     */
    public function getPermissions()
    {
        return $this->permissions;
    }

    /**
     * @return string
     */
    public function getLogin()
    {
        return $this->login;
    }
}
