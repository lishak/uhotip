<?php
namespace App\Security;

class AccessList extends \Nette\Security\Permission
{
    public function __construct()
    {
        $this->addRole('guest');
        $this->addRole('user', 'guest');
        $this->addRole('admin', 'user');
        $this->addRole('superadmin', 'admin');
        //RESOURCES
        $this->addResource('Sign');
        $this->addResource('Homepage');
        $this->addResource('Bet:Homepage');
        //ROLE GUEST
        $this->allow('guest', 'Sign', array('in'));
        //ROLE USER
        $this->allow('user', 'Homepage', self::ALL);
        //ROLE SUPERADMIN
        $this->allow('superadmin', self::ALL, self::ALL);
    }
}
