<?php
namespace App\Security;

use Nette\Security\IAuthorizator;
use Nette\Object;

class AccessControlList extends Object
{
    /** @var \Nette\Database\Context */
    protected $database;
    /** @var \Nette\Security\User */
    protected $user;
    /** @var AccessList */
    protected $accessList;

    public function __construct(
        \Nette\Database\Context $database,
        \Nette\Security\User $user,
        \App\Security\AccessList $accessList
    ) {
        $this->database = $database;
        $this->user = $user;
        $this->accessList = $accessList;
    }

    /**
     * @param string $resource
     * @param string $action
     * @return bool
     */
    public function isAllowed($resource, $action)
    {
        /** @var \App\Security\Identity $identity */
        $identity = $this->user->getIdentity();
        $permissions = $identity->getPermissions();

        if (isset($permissions[$resource][$action])) {
            return $permissions[$resource][$action];
        }

        return $this->accessList->isAllowed($identity->getRole(), $resource, $action);
    }
}
